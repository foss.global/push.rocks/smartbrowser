/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartbrowser',
  version: '2.0.5',
  description: 'simplified puppeteer'
}
