export interface IScreenShotResult {
  name: string;
  id: string;
  buffer: Buffer;
}
