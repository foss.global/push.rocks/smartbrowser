import { tap, expect, expectAsync } from '@pushrocks/tapbundle';

import * as smartbrowser from '../ts/index.js';
let testSmartBrowser: smartbrowser.SmartBrowser;

tap.test('should instanstiate a new browser ', async () => {
  testSmartBrowser = new smartbrowser.SmartBrowser();
  return expect(testSmartBrowser).toBeInstanceOf(smartbrowser.SmartBrowser);
});

tap.test('should start the browser ', async () => {
  await testSmartBrowser.start();
});

tap.test('should create a PDF from a page', async (tools) => {
  const result = await testSmartBrowser.pdfFromPage('https://lossless.com');
  expect(result.buffer).toBeInstanceOf(Buffer);
});

tap.test('should produce a valid screenshot', async (tools) => {
  const result = await testSmartBrowser.screenshotFromPage('https://lossless.com');
  expect(result.buffer).toBeInstanceOf(Buffer);
});

tap.test('should evalute something in the browser', async () => {
  const result = await testSmartBrowser.evaluateOnPage('https://lossless.com', async () => {
    return window.location.toString();
  });
  console.log(result);
});

tap.test('should stop the browser ', async () => {
  await testSmartBrowser.stop();
});

tap.start();
